# Production Readiness Guide

For any new or existing system or a large feature set the questions in this guide help make the existing service more robust, and for new systems it helps prepare them and speed up the process of becoming fully production-ready.

Initially, this guide is likely to be used by Production Engineers who are embedded with other teams working on existing services and features. However, anyone working on a new service or feature set is encouraged to use this guide as well.

The goal of this guide is to help others understand how the new service or feature set may impact the rest of the (production) system; what steps need to be taken (besides deploying this new system) to ensure that it can be properly managed; and to understand what it will take to manage the reliability of the new system / feature / service beyond its' initial deployment.


## Summary

The next service that is part of the effort to [migrate GitLab.com to Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112) is [git https and websockets](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/228).

This is the first Rails front-end service that will be migrated to Kubernetes, the first service where we will be processing traffic with Puma and an NGINX ingress.

The benefit to GitLab and our customers are the following:

* We will be adding to our existing Kubernetes deployment with additional use of the [GitLab Helm chart](https://gitlab.com/gitlab-org/charts/gitlab)
* We expect more efficient use of resource due to horizontal pod auto-scaling, where currently all Git HTTPs and Websocket traffic is serviced by a static fleet of virtual machines
* Deploy times for service will be reduced, this is due to us using pre-built docker images for deployment vs installing the omnibus package which contains the entire set of binaries to run GitLab, and using pod disruption budget to more efficiently upgrade the fleet without service interruption

## Architecture

This review is for migrating the existing Websockets and Git HTTPs service to Kubernetes. There are no configuration changes as part of the migration, beyond running the service in containers in GKE.

This sequence diagram illustrates a basic Git clone using https:

### Git HTTPS

Example of a git clone using Git HTTPS:

```mermaid
  sequenceDiagram
      participant Client
      participant NGINX
      participant Workhorse
      participant Rails
      participant Gitaly

      Client->>NGINX: git clone
      NGINX->>Workhorse: get info/refs?service=git-upload-pack
      Workhorse->>Rails: auth check
      Rails->>Workhorse: auth ok
      Workhorse->>Gitaly: gitaly-upload-pack
      Gitaly->>Workhorse: git data
      Workhorse->>Client: git data
```

Notes:

* Rails has dependencies on Redis/Postgres
* Gitaly requests may be routed through praefect depending on the project

### Websockets

Currently, the only application for Websockets is the web terminal for in-terminal access to an environment. See more details in https://gitlab.com/gitlab-org/gitlab-workhorse/blob/master/doc/channel.md

 ```mermaid
     sequenceDiagram
        participant Client
        participant NGINX
        participant Workhorse
        participant Rails
        participant Kubernetes API

        Client->>NGINX: websocket connection
        NGINX->>Workhorse: websocket connection
        Workhorse->>Rails: connection details and user permissions
        Rails->>Workhorse: auth ok
        loop ConnectionDetails
            Workhorse->>Rails: connection details and permissions
        end
        Workhorse->>Kubernetes API: websocket frames for terminal access
        Kubernetes API->>Workhorse: websocket frames for terminal access
        Workhorse->>Client: websocket terminal
  ```

Notes:

* The Kubernetes cluster referred to here is the **customer's cluster attached to GitLab**, not the cluster servicing GitLab.com
* Polling for Kubernetes connection details and access is initiated by Rails via a Sidekiq job


### GKE Cluster and current dependencies

The following diagram outlines the current production GKE cluster configuration and all dependencies.


* **Green items are those that are being introduced in this configuration change**
* **Blue items are existing VM infrastructure that we now depend on with this change**
* `Webservice` refers to the pod that is running Rails in the cluster, this pod is servicing Git HTTPs and Websockets

We are introducing the following **new** dependencies by moving Git HTTPS / Websockets into the cluster:

* Consul agent with a connection to our existing Consul cluster
* Connections to the PosgreSQL replicas, previously we only were using the primary PgBouncer

```plantuml
left to right direction
skinparam roundcorner 20
skinparam shadowing false
skinparam arrowColor Olive
skinparam rectangle {
  BorderColor DarkSlateGray
}

rectangle "GKE Cluster" as GKE {
  card "gitlab" as GPRD {
    rectangle "Registry" as GPRD_REGISTRY
    rectangle "Sidekiq-*" as GPRD_SIDEKIQ
    rectangle "Nginx-ingress" as GPRD_NGINX
    rectangle "Mailroom" as GPRD_MAILROOM
    rectangle "Webservice" AS GPRD_WS #LightGreen
  }

  card "gitlab-cny" as GPRD_CNY {
    rectangle "Nginx-ingress" as GPRD_CNY_NGINX
    rectangle "Registry" as GPRD_CNY_REGISTRY
    rectangle "Webservice" AS GPRD_CNY_WS
  }
  card "consul" as GKE_CONSUL  #LightGreen {
    rectangle "Consul-dns" as GKE_CONSUL_DNS  #LightGreen
  }


  card "monitoring" as MON {

    rectangle "Cloudflare-exporter" as MON_CLOUDFLARE
    rectangle "Gitaly-exporter" as MON_GITALY
    rectangle "Node-exporter" as MON_NODE

    rectangle "Prometheus-operator" as MON_PROM
    rectangle "Thanos-query" as MON_THANOS
  }

  card "plantuml" as UML {
    rectangle "Plantuml" as PUML
  }
  card "logging" as LOG {
    rectangle "Fluentd-Elasticsearch" as FLUENTD
  }

}

rectangle "Object Storage" as OS
rectangle "Virtual Machines" as VMS {
  rectangle "Redis" as REDIS {
    rectangle "Redis-sidekiq" as REDIS_SIDEKIQ
    rectangle "Redis-cache" as REDIS_CACHE
    rectangle "Redis-persistent" as REDIS_PERS
  }
  rectangle "GitLab.com /api" as GLAPI

  rectangle "Gitaly" as GITALY

  rectangle "Consul cluster" as CONSUL #LightSkyBlue {
    rectangle "Consul x3" as C1
  }

  rectangle "Postresql" as PG {
    rectangle "PgBouncer (primary)" as PG_PGBOUNCER
    rectangle "replicas X 7" as PG_REPLICAS #LightSkyBlue
  }
}

card "GitLab.com Load Balancer" as LB {
  rectangle "https_git" as HTTPS_GIT #LightSkyBlue
  rectangle "canary_https_git" as CNY_HTTPS_GIT #LightSkyBlue
}

GKE_CONSUL -[#LightGreen]- CONSUL
GPRD_REGISTRY -- GLAPI
GPRD_REGISTRY -- OS
GPRD_CNY_REGISTRY -- GLAPI
GPRD_CNY_REGISTRY -- OS

GPRD_SIDEKIQ -- GITALY
GPRD_SIDEKIQ -- REDIS
GPRD_SIDEKIQ -- PG_PGBOUNCER

GPRD_WS -[#LightGreen]- GITALY
GPRD_WS -[#LightGreen]- GKE_CONSUL_DNS
GPRD_WS -[#LightGreen]- REDIS
GPRD_WS -[#LightGreen]- PG_REPLICAS
GPRD_WS -[#LightGreen]- PG_PGBOUNCER
GPRD_NGINX -[#LightGreen]- HTTPS_GIT

GPRD_CNY_WS -[#LightGreen]- GITALY
GPRD_CNY_WS -[#LightGreen]- GKE_CONSUL_DNS
GPRD_CNY_WS -[#LightGreen]- REDIS
GPRD_CNY_WS -[#LightGreen]- PG_REPLICAS
GPRD_CNY_WS -[#LightGreen]- PG_PGBOUNCER
GPRD_CNY_NGINX -[#LightGreen]- CNY_HTTPS_GIT
```

### Existing VM configuration

Currently the git-xx Virtual Machines service Git HTTPS, Websocket, and Git SSH traffic.
This migration will first only move Git HTTPS and Websocket traffic, leaving Git SSH traffic to be processed by VMs until we migrate gitlab-shell into the cluster.

* In production, there are 24 Virtual Machines (and 2 canary VMs) with 16 cores in 20GB of memory
* NGINX and Workhorse run on each VM, alongside Puma/Rails which is configured with 16 workers
* The total number of puma workers in production is 384 not counting canary since it currently receives very little traffic

### Proposed CloudNative configuration

Initially, the GKE configuration for this workload will be the following:

* Dedicated node pool with a minimum of 3 Virtual Machines (1 per zone), each having 16 cores and 20GB of memory
* To match the number of workers in production, we will set
  * minimum of 150 pods in production, ensuring a minimum capacity of 300 puma workers
  * minimum of 10 pods in canary, ensuring a minimum capacity of 20 workers for up to 5% of https traffic

#### Performance and load testing

_See details in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1067_

In production, the typical number of requests per puma worker for Git HTTPs is 3 req/worker/sec.

For the 2 worker/pod configuration, we see a typical CPU utilization of .3 to .4 cores and 1.5 to 2GB of memory utilization for the puma container.

#### Puma Requests and Limits

* workerMaxMemory: 1342MB
* min/max threads: 1/4
* CPU requests: 2
* CPU limits: 2
* memory requests: 2.5G
* memory limit: 3G

#### Workhorse Requests

* CPU requests: 300m
* memory requests: 100M

#### NGINX ingress controller Requests

* CPU requests: 100m
* memory requests: 100Mi

#### PDB Config

Pod disruption currently use the GitLab Chart defaults:

```
NAME                                      MIN AVAILABLE   MAX UNAVAILABLE   ALLOWED DISRUPTIONS   AGE
gitlab-nginx-ingress-controller           2               N/A               1                     21d
gitlab-nginx-ingress-default-backend      1               N/A               1                     21d
gitlab-webservice                         N/A             1                 1                     21d
```
#### HPA

**Note**: These values are initially chosen and will likely change as we move to production, the SSOT for HPA configuration is in the [values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/values.yaml.gotmpl) and environment specific [gprd.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/gprd.yaml.gotmpl) for production

```
NAME                                      REFERENCE                                            MINPODS   MAXPODS
gitlab-nginx-ingress-controller           Deployment/gitlab-nginx-ingress-controller           1         20
gitlab-webservice                         Deployment/gitlab-webservice                         150       300
```

### Migration Plan

#### Current Virtual Machine configuration with HAProxy

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
  card "https_git" as A1
  card "canary_https_git" as A2
  card "websocket" as A3
  card "canary_websocket" as A4
}
rectangle "git-XX VMs" as B {
  card "canary" as B1 {
      rectangle "nginx/workhorse/puma" as B11
  }
  card "main" as B2 {
      rectangle "nginx/workhorse/puma" as B21
   }
}

A1 --> B21 : main
A3 --> B21 : main
A2 --> B11 : cny
A4 --> B11 : cny
```

The Canary stage for websockets and Git HTTPs is routed based on request path, for a static set of groups. This currently does not receive much traffic and part of the migration will be increasing this load on canary, by taking a small percentage of traffic and diverting it to the canary stage.

#### Phase 1

The first phase will be to move a portion of our traffic to the canary namespace in the Kubernetes cluster.
The canary VMs will be replaced with the canary namespace in the cluster.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
      rectangle "https_git backend" as A1
      rectangle "canary_https_git backend" as A2
      rectangle "websockets" as A3
      rectangle "canary_websockets" as A4
    }
    rectangle "git-XX VMs" as B {
      card "main" as B2 {
          rectangle "nginx/workhorse/puma" as B21
       }
    }
    rectangle "Production GKE" as C {
      card "gitlab-cny namespace" as C1 {
          card "webservice pod" as C11 {
            rectangle "puma" as C111
            rectangle "workhorse" as C112
          }
          rectangle "nginx ingress" as C12
       }

      card "gitlab namespace" as C2 {
          card "webservice pod" as C21 {
            rectangle "puma" as C211
            rectangle "workhorse" as C212
          }
          rectangle "nginx ingress" as C22
       }
    }

    A1 --> B21
    A3 --> B21
    A2 --> C12
    A4 --> C12
    A1 --> C12 : 0-5%
    A3 --> C12 : 0-5%
```

* We will slowly take up to 5% of traffic from the main stage and divert it to the Canary namespace in the GKE cluster.
* The `canary_https_git` backend receives very little traffic, it currently is namespaced to only certain request paths
* The `canary_websockets` and `websockets` backed is only used for the interactive terminal and receives very little traffic


#### Phase 2

Once we are confident in taking 5% of https traffic to the gitlab-cny namespace, we will start divert all traffic over the cluster, after this transition is complete, the VMs will only be processing git-ssh traffic and can be scaled down.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
      rectangle "https_git backend" as A1
      rectangle "canary_https_git backend" as A2
      rectangle "websockets" as A3
      rectangle "canary_websockets" as A4
    }
    rectangle "git-XX VMs" as B {
      card "main" as B2 {
          rectangle "nginx/workhorse/puma" as B21
       }
    }
    rectangle "Production GKE" as C {
      card "gitlab-cny namespace" as C1 {
          card "webservice pod" as C11 {
            rectangle "puma" as C111
            rectangle "workhorse" as C112
          }
          rectangle "nginx ingress" as C12
       }

      card "gitlab namespace" as C2 {
          card "webservice pod" as C21 {
            rectangle "puma" as C211
            rectangle "workhorse" as C212
          }
          rectangle "nginx ingress" as C22
       }
    }

    A2 --> C12
    A4 --> C12
    A1 --> C12 : 5%
    A3 --> C12 : 5%
    A1 --> C22 : 0-95%
    A3 --> C22 : 0-95%
```

### Network

The migration from Virtual Machines to GKE introduces additional network hops and cross-zone traffic that will need to be considered as it relates to complexity and cost.


#### Virtual Machine Network Topology

We currently run the `git-*` fleet on Virtual Machines that are directly attached to HAProxy VMs. Recently, we configured HAProxy so that VMs in the same availability zone are preferred which helped to reduce our cloud cost for cross-zone network traffic. Git HTTPs traffic represents a significant amount of network ingress and egress.

```plantuml
skinparam roundcorner 20
skinparam shadowing false

note "1 point of cross-AZ network traffic" as N1
cloud "Public Internet" as PI
rectangle "TCP LB :443" as TCP_LB

folder "us-east1-b" as F1B {

  rectangle "HAProxy" as HAProxyB
  storage "Git VM fleet" as GitB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  storage "Git VM fleet" as GitC

}


folder "us-east1-d" as F1D {

  rectangle "HAProxy" as HAProxyD
  storage "Git VM fleet" as GitD

}


folder "us-east1-b" as F2B {

  storage "Gitaly fleet" as GitalyB

}

folder "us-east1-c" as F2C {

  storage "Gitaly fleet" as GitalyC

}
folder "us-east1-d" as F2D {

  storage "Gitaly fleet" as GitalyD

}

PI -- TCP_LB
TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD
HAProxyB -- GitB
HAProxyC -- GitC
HAProxyD -- GitD

GitB -- GitalyB
GitB -- GitalyC
GitB -- GitalyD
GitC -- GitalyB
GitC -- GitalyC
GitC -- GitalyD
GitD -- GitalyB
GitD -- GitalyC
GitD -- GitalyD
```

Notes:

* Lines that cross availability zones are where we are billed at a higher rate
* In our current configuration there is **1 point** of cross-zone network traffic, it occurs between the `git-*` fleet and the Gitaly servers.
* We are unable to prefer Gitaly servers in the same availability zone because requests are round-robined to the `git-*` fleet and repositories are not currently deployed redundantly across AZs

### GKE Network Topology for Webservice


```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI

note "3 points of cross-AZ network traffic" as N1

rectangle "GitLab.com TCP LB :443" as TCP_LB

PI -- TCP_LB

folder "us-east1-b" as F1B {
  rectangle "HAProxy" as HAProxyB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
}


folder "us-east1-d" as F1D {
  rectangle "HAProxy" as HAProxyD
}

TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD

rectangle "Internal GKE TCP LB :443" as GKE_LB

HAProxyB -- GKE_LB
HAProxyC -- GKE_LB
HAProxyD -- GKE_LB

folder "us-east1-b" as F2B {

  node "nginx ingress" as NGINXB
}

folder "us-east1-c" as F2C {

  node "nginx ingress" as NGINXC

}
folder "us-east1-d" as F2D {

  node "nginx ingress" as NGINXD

}

GKE_LB -- NGINXB
GKE_LB -- NGINXC
GKE_LB -- NGINXD

folder "us-east1-b" as F3B {
  node "webservice pod" as WebB
}

folder "us-east1-c" as F3C {

  node "webservice pod" as WebC

}
folder "us-east1-d" as F3D {

  node "webservice pod" as WebD

}

NGINXB -[#MidnightBlue]- WebB
NGINXB -[#MidnightBlue]- WebC
NGINXB -[#MidnightBlue]- WebD


NGINXC -[#MidnightBlue]- WebB
NGINXC -[#MidnightBlue]- WebC
NGINXC -[#MidnightBlue]- WebD

NGINXD -[#MidnightBlue]- WebB
NGINXD -[#MidnightBlue]- WebC
NGINXD -[#MidnightBlue]- WebD


folder "us-east1-b" as F4B {

  storage "Gitaly fleet" as GitalyB

}

folder "us-east1-c" as F4C {

  storage "Gitaly fleet" as GitalyC

}
folder "us-east1-d" as F4D {

  storage "Gitaly fleet" as GitalyD

}

WebB -- GitalyB
WebB -- GitalyC
WebB -- GitalyD

WebC -- GitalyB
WebC -- GitalyC
WebC -- GitalyD

WebD -- GitalyB
WebD -- GitalyC
WebD -- GitalyD
```

Notes:

* In our current configuration, there are **3 points** of cross AZ ingress/egress
* After the transition, NGINx will no longer be running on the same Virtual Machine (Node) as the webservice pod
* This means that the connection between NGINX and Webservice (workhorse/puma) changes from a local unix socket ` unix:/var/opt/gitlab/gitlab-workhorse/socket` to a TCP connection
* The connection between NGINX and the Webservice pod is not encrypted (highlighted in blue)

## Operational Risk Assessment

## What are the internal and external dependencies of this service, are there SPOFs?

### Consul

This introduces a new dependency to the cluster, consul which is installed in the Cluster. Consul and Consul-DNS is used for database [service discovery](https://docs.gitlab.com/ee/administration/database_load_balancing.html#service-discovery) to fetch the list of replicas.  In the situation where Consul is completely removed, the application will gracefully revert to only using the Primary database.

For example, deleting the Consul service will result in these application errors:

```
Service discovery encountered an error: No response from nameservers list
Sending event 1b60bcdb190144479175dda4a79bdd30 to Sentry
```

But the application will still function, though more pressure will be put on the primary DB.

### Postgres Replicas

Moving this service into Kubernetes means that the cluster now has a dependency on Postgres Replicas, which are discovered using Consul DNS as explained above. There is an outstanding issue where a severed network link to any of the replicas will cause a [catastrophic failure of the Application](https://gitlab.com/gitlab-org/gitlab/-/issues/238556).

### Existing dependencies outside of the cluster

The following dependencies are dependencies of the new service, but are already present for Sidekiq

* Redis (persistent/cache/sidekiq)
* Patroni / PgBouncer
* HAProxy
* Gitaly

## What are the potential scalability or performance issues that may result with this change?

### Scaling

There is a risk that we will not be able to adapt quickly enough to increases in traffic, or sudden increases will cause saturation on the node. To mitigate this we have:

* To start, we are slightly over-provisioning and setting a generous reserved capacity of pods
* Isolating this workload in its own node-pool

### Log volume

We will mostly be using the same log indexes when running in Kubernetes, with some small changes:

* All logs from the webservice container will be forwarded to the rails index. This includes both puma and rails, and all unstructured logs present in `/var/log/gitlab/*`.
* All logs from the workhorse container will be forwarded to the workhorse index
* There are additional unstructured logs that will be forwarded to the ElasticSearch cluster that we are currently ignoring on VMs. We don't believe this will cause too much additional load on the cluster, and we have [disabled the unstructured production.log and application.log](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/347).

We don't expect any performance issues due to logging, but will be monitoring this closely during the migration. Note that we expect to see a different rate of `/-/readiness` check log messages, due to the reconfiguration of HAProxy and readiness requests in Kubernetes.

### Prometheus metrics

There are [discussions](https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2212#note_391344422) about how will shard Prometheus as we start to collect more metrics in the cluster. We don't currently believe there will be any loading issue on Prometheus in cluster for this service transition, but will be monitoring it closely.


## List the top three operational risks when this feature goes live.

* Unexpected configuration differences between Cloud Native and Omnibus. We have done our best to mitigate this risk by auditing configuration in Staging https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1065
* Issues with HPA, not enough reserved capacity and saturation problems related to scaling
* Disruption and user impact during upgrades due to long-lived connections
* Issues with metrics overwhelming the current in-cluster Prometheus

## Security and Compliance

* Network limits that prevent access to the internal network using [a network policy](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/6423e160a56fd35f9c961f39d7b4c5d1248c2294/releases/gitlab/values/values.yaml.gotmpl#L594-671)

## Performance

### Are there any throttling limits imposed by this feature? If so how are they managed?

We use the same rack attack configuration in Kubernetes that is present on Omnibus VMs:

```
  rack_attack:
    git_basic_auth:
      bantime: 3600
      enabled: true
      findtime: 180
      ip_whitelist:
      - 127.0.0.1
      maxretry: 30
```

## Monitoring and Alerts

During the migration, we will be monitoring the following metrics and logs:

* [Git overview dashboard](https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd), closely monitoring errors, apdex, and RPS
* [Pod overview dashboard](https://dashboards.gitlab.net/d/git-pod/git-pod-info?orgId=1)
* [GKE Workloads](https://console.cloud.google.com/kubernetes/workload?project=gitlab-production)
* Request durations from logs for rails https://log.gprd.gitlab.net/goto/36cd553b6df5cf9cb68a9b87215fc1eb
* Request durations from logs for workhorse https://log.gprd.gitlab.net/goto/838d99a42154be2d44fb689e8db67ed0

## Testing

See testing that was done in Staging under load in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1067

## Known issues going into production

There are a few known issues that will need to be addressed soon after we migrate the service:

* proxy_request_buffering should be disabled for some paths the nginx controller
  * https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2262
  * We are globally disabling `proxy_request_buffering` until we have a way to selectively turn it off in the GitLab chart for specific request paths
* Missing configuration related to incoming mail and CSP for Rails that should not impact these two services:
  * https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2256
  * https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2257
  * https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2255
* Allowing additional front-end services after git-https
  * https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2212
  * This is a blocker for additional HTTPs services, after the git-https migration is complete
