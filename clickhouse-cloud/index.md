
The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here:

data_stores entry addition is being tracked here https://gitlab.com/gitlab-org/gitlab/-/issues/427656
  - Link to or provide a high-level summary of this new product feature.

  There is a growing need for a highly-scalable, resilient, and OLAP-capable datastore for GitLab.com. After discussion, [ClickHouse Cloud GCP](https://clickhouse.com/cloud) was [selected as the best fit in supporting key project](https://about.gitlab.com/company/team/structure/working-groups/clickhouse-datastore/#context), see [epic](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34299) for further background.

  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.

  We interact with ClickHouse Cloud through an internally-developed HTTP client using POST requests. The client is part of the GitLab application codebase and uses our existing HTTP client-related tooling: [`Gitlab::HTTP`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/http.rb)

  The client code has no direct connection to the GitLab database (PostgreSQL), it's the developer's responsibility to implement data access or sync as they see it fit. A usual interaction might look like this:

  INSERT: Use Sidekiq to query data from PostgreSQL, buffer and transform the results in Ruby and invoke the ClickHouse Client to insert the data. The developer is responsible to measure saturation of used systems and when necessary throttle the generated load. 

  SELECT: User navigates to the Contribution Analytics UI (or other UI using ClickHouse) and the Rails backend will invoke a ClickHouse query and present the results.

  The client uses HTTPS with gzip compression enabled. Stored/synced data is not encrypted. You can see more information about the ClickHouse Cloud infrastructure here: https://clickhouse.com/docs/en/cloud/reference/architecture

  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.

  See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34299#initial-projects-planning-to-use

  - List individuals are the subject matter experts and know the most about this feature.

  https://gitlab.com/groups/gitlab-org/maintainers/clickhouse/-/group_members?with_inherited_permissions=exclude

  https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles/-/issues/71

  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.

  https://gitlab.com/groups/gitlab-org/maintainers/clickhouse/-/group_members?with_inherited_permissions=exclude

  - List the member(s) of the team who built the feature will be on call for the launch.

  NA, team will follow the standard on-call incident management process staffed by Development and Infrastructure team members.

  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.

  NA

### Infrastructure

Section not applicable (N/A) since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc.  See https://trust.clickhouse.com/.

_The items below will be reviewed by the Reliability team._

- [ ] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
- [ ] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?

GCP load balancers

- [ ] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] List the top three operational risks when this feature goes live.


1. ClickHouse Cloud instance going down
1. Data replication failing/slow resulting in new data not being available to query
1. High load/instability from increased read operations against Postgres (syncing data to ClickHouse)

- [ ] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

The features we plan to implement will have existing implementations using PostgreSQL which means we'll have the ability to enable/disable ClickHouse usage via feature flags.

**Scenario 1: slow response from ClickHouse Cloud.**

Slow response from ClickHouse Cloud could affect web nodes (response time) for specific page visits.
A similar scenario could also affect the Sidekiq nodes however, the severity is much smaller here. We're planning to use periodical jobs where we add strict runtime limits and ensure that there is only one sync job running (per table) at a time.

**Scenario 2: data loss from ClickHouse Cloud.**

The data/tables we're planning to sync are already present in PostgreSQL. Lost data could be re-synced from PostgreSQL.

**Scenario 3: Falling back to PostgreSQL.**

Contribution Analytics was originally implemented using PostgreSQL and the database queries on the page cannot be optimized further. For very large groups, the queries are very close to the statement timeout threshold. The ClickHouse-based implementation provides much better performance so in case there is a switch back to PostgreSQL we should see slow response times again.  

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

As a new datastore, ClickHouse will be primarily monitored via performance metrics ensuring availability and usage are within expected parameters. See documentation for monitoring overview.

This work is being tracked in this issue https://gitlab.com/gitlab-org/gitlab/-/issues/427657

The following are key errortracking API metrics to be monitored:

- HTTP latency
- DB request latency
- HTTP requests per second
- DB requests per second
- Error rate
- Saturation


The following are key Clickhouse metrics to be monitored:
- RSS/Mapped memory usage (via [system.asynchronous_metrics](https://clickhouse.com/docs/en/operations/system-tables/asynchronous_metrics/#system_tables-asynchronous_metrics))
- Disk usage (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Data volume to replicas (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Active background tasks (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Number of connections to HTTP server (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Total executing query count (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Total background merge count (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- [Load on processors](https://clickhouse.com/docs/en/operations/monitoring/#resource-utilization)

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

See [epic](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34299) for a list of initial projects planning to use ClickHouse Cloud. Each project will have its own rollout issue.

- [ ] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

Yes, all data reading from ClockHouse Cloud will be built behind a feature flag. If necessary the feature flag can be switched so that the product will once again read data from the Postgres database.

| Feature Flag | Rollout issue | Function | Comment |
| ----------- | ----------- | ----------- | ----------- |
| generate_ci_finished_builds_sync_events | https://gitlab.com/gitlab-org/gitlab/-/issues/424866 | prepares data in postgres | controls filling a new table in postgres database. It's already been enabled on 2024-10-02 and seems to be fine. We would disable it if we wanted to reduce the load on Postgres. But it also effectively stops new data from being uploaded to clickhouse. |
| ci_data_ingestion_to_click_house | https://gitlab.com/gitlab-org/gitlab/-/issues/424498 | Postgres -> ClickHouse sync | controls actual data ingestion to ClickHouse. We would disable this if we see that we overload ClickHouse itself. |
| clickhouse_ci_analytics | https://gitlab.com/gitlab-org/gitlab/-/issues/424866 | API and UI using ClickHouse | disables API and part of the admin dashboard that uses clickhouse. This FF is mostly to not show untested functionality on self-managed. |
| runners_dashboard | https://gitlab.com/gitlab-org/gitlab/-/issues/417002 | UI for bigger dashboard | that one disables the whole dashboard. |
| sync_audit_events_to_clickhouse | https://gitlab.com/gitlab-org/gitlab/-/issues/427738 | Starts syncing of audit events being generated into ClickHouse |  |
| migrate_audit_events_to_clickhouse | https://gitlab.com/gitlab-org/gitlab/-/issues/427739 | Migrate current rows of audit events table from Postgres to ClickHouse |  |
| read_audit_events_from_clickhouse | https://gitlab.com/gitlab-org/gitlab/-/issues/427741 | Switches database for reading audit events from Postgres to ClickHouse |  |


- [ ] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).

NA, since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [ ] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: No
  - New Subnets: No
  - VPC/Network Peering: No
  - DNS names: No - DNS is provided by Clickhouse.
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...): No. ClickHouse Cloud is accessed over a private connection using [GCP's Private Service Connect](https://clickhouse.com/docs/en/manage/security/gcp-private-service-connect) offering (see: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24569).
  - Other (anything relevant that might be worth mention): No
- [ ] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?

NA, since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc. See https://trust.clickhouse.com/.

- [ ] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.

NA, since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc. See https://trust.clickhouse.com/.

- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?

ClickHouse Cloud provides options to scale and replicate the instances. Additionally, the implemented HTTP client allows interacting with multiple databases so at some point we could decompose the data if needed.

- [ ] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.

Terraform setup is being configured and tracked in this issue https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24568

- [ ] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?

NA, since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc. See https://trust.clickhouse.com/.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [ ] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?

Yes - ClickHouse database user accounts will need to be created for production, staging, etc.  See https://clickhouse.com/docs/en/cloud/users-and-roles.  

We will be using Google SSO for authentication (https://gitlab.com/gitlab-com/it/engops/issue-tracker/-/issues/285#note_1450176677)

Authentication for ClickHouse is being configured here https://gitlab.com/gitlab-org/charts/gitlab/-/issues/4924 and credentials are available in 1 password vault.


- [ ] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?

TBD - Development is progress and ClickHouse database users should be confirmed to follow this principle.  

- [ ] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?

Yes - Network access is limited via IP Access Lists and should be limited to production IPs for RED data usage.  See https://clickhouse.com/docs/en/manage/security/ip-access-list. 

- [ ] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

No. Network access is limited via IP Access Lists and should be limited to production IPs for RED data usage.  See https://clickhouse.com/docs/en/manage/security/ip-access-list

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [ ] Did we make an effort to redact customer data from logs?

We will be logging queries, excluding senstive information https://gitlab.com/gitlab-org/gitlab/-/issues/421403

- [ ] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

Data will be stored in ClickHouse Cloud which is backed by GCP Object Storage. Object storage and bucket creation is managed by the service. See https://clickhouse.com/docs/en/cloud/reference/architecture.

- [ ] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?

RED. Storage of RED data has been approved by Security and Privacy teams.  See https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/125844.

- [ ] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.

ClickHouse [supports activity logs](https://clickhouse.com/docs/en/manage/security/organization-activity)

This work is being tracked in this issue https://gitlab.com/gitlab-org/gitlab/-/issues/427658

 - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.

 ClickHouse [supports activity logs](https://clickhouse.com/docs/en/manage/security/organization-activity)

 - [ ] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

NA, since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc. See https://trust.clickhouse.com/. See https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/125844+

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to examples of logs on https://logs.gitlab.net

ClickHouse metrics will be sent to Prometheus for our production and staging instances. Instructions on setup here https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34299#note_1565357812

To some extent, yes. ClickHouse requests should be also included into the `gitlab_external_http_*` metrics.

- [ ] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there custom backup/restore requirements?
- [ ] Are backups monitored?
- [ ] Was a restore from backup tested?
- [ ] Link to information about growth rate of stored data.

ClickHouse Cloud [supports back and restore](https://clickhouse.com/docs/en/operations/backup)

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

NA, since ClickHouse cloud is a managed infrastructure service developed and hosted by ClickHouse Inc

- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.

Yes, all data reading from ClockHouse Cloud will be built behind a feature flag. If necessary the feature flag can be switched so that the product will once again read data from the Postgres database.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call engineers have access to this service.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).

There have been several examples, PoCs where we've seen significantly better database query performance compared to PostgreSQL.

The ClickHouse optimal database schema and queries were determined through research issues where we used large volume of production-like data on local or CH cloud instances. For measuring the performance we used the standard tooling provided by ClickHouse, the `EXPLAIN` statement. Example: https://gitlab.com/gitlab-org/gitlab/-/issues/414934

We also reached out Clickhouse Inc and GitLab team members with ClickHouse expertise to review the designs.

- [ ] Link to any load testing plans and results.

Test plan (for Contribution Analytics):

1. Load `events` data into ClickHouse Cloud.
2. Start N parallel processes where `SELECT` queries are executed for specific groups (large, small).
3. Monitor the response times.
Test plan (for Compliance): 

1. Load test data into the ClickHouse cloud.
2. Simulate read and write operations, we should also test complex `SELECT` queries which would otherwise fail on our current PostgreSQL database. 
3. Begin with a low load and gradually increase it until we reach or exceed our current load.
4. Actively monitor ClickHouse metrics, throughput, and response time.

More details can be found in this issues https://gitlab.com/gitlab-org/gitlab/-/issues/422607 & https://gitlab.com/gitlab-org/gitlab/-/issues/422607

- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?

Very minor. The periodical sync jobs will read data from PostgreSQL in batches (likely using the replicas).

- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
