# about.gitlab.com

## Summary

Our company website, about.gitlab.com, is a static website hosted in Google Cloud Storage (GCS). It could theoretically
be served directly out of GCS, however we have many
[redirects](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/redirects.yml) that we need to handle so we
leverage Cloudflare Workers to process the requests.

[Cloudflare Workers](https://workers.cloudflare.com/) is a serverless platform that spawns a worker (running our
[code](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/redirects/cloudflare-wrangler/src/main.ts)) for every
request. This allows us to process these requests efficiently without having to worry about performance,
scaling & reliability concerns. For each request, the code checks to see if it matches one of our redirects, and if it
does not, then it will serve the content from cache/GCS (or return a 404).

The content for the site [about.gitlab.com](https://about.gitlab.com) is made up from several repositories such as:

- [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
- [buyer-experience](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience)
- [gitlab-blog](https://gitlab.com/gitlab-com/marketing/digital-experience/gitlab-blog) - WIP

The main repo is `www-gitlab-com` and this is where we store the [Cloudflare Worker
code](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/redirects/cloudflare-wrangler/src/main.ts),
[redirects](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/redirects.yml) and CI pipelines that deploy
to Cloudflare.

## Architecture

Traffic for about.gitlab.com and *.about.gitlab-review.app is routed the same way:

1. Request hits Cloudflare
1. Cloudflare finds matching Worker route
1. Cloudflare fires up a Worker to process the request
1. Worker processes request by either redirecting or fetching the content from cache/GCS (or returning a 404).

The Worker deployment contains a copy of the
[`redirects.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/redirects.yml) and it is parsed &
evaluated to determine if requests match one of the redirects.

### Review Apps

We support the following via `www-gitlab-com` review environments:

1. Testing changes to `data/redirects.yml`
1. Testing changes to Cloudflare Worker code

The [Cloudflare Worker code](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/redirects/cloudflare-wrangler)
would typically reference its embedded copy of the `data/redirects.yml` file (copied from the `www-gitlab-com` repo) to
determine if a request matches a redirect.
However, to test changes to `data/redirects.yml`, if the MR contains a change to this file then the CI job pushes the
contents of the `data/redirects.yml` file into [Cloudflare Workers
KV](https://www.cloudflare.com/developer-platform/workers-kv/). The Cloudflare Worker code will first query the KV store
for the modified `data/redirects.yml` file for that MR and if it exists, it will use that instead of the embedded file.
This allows testing of redirects within the review environment in question without affecting other review environments.

We also support testing changes to the [Cloudflare Worker
code](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/redirects/cloudflare-wrangler) within the review
environment. If any changes are made to the
[code](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/redirects/cloudflare-wrangler), then we will deploy a
separate Cloudflare Worker specifically for that MR allowing you to test your changes within the review environment
without affecting other review environments.

See <https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/about-gitlab-com.md#review-apps> for more
info.

### Production

For production, the code bypasses any interaction with Cloudflare Workers KV and only uses the embedded
`data/redirects.yml` file.

See <https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/about-gitlab-com.md#production> for more
info.

## Operational Risk Assessment

This service relies exclusively on Cloudflare features (the platform itself, workers, KV) and Google Cloud Storage (GCS)
for any content that we don't return from cache.

## Security and Compliance

Unfortunately due to Cloudflare's lack of granularity on their access controls, it is not currently possible to limit
the scope of these API variables to `about.gitlab.com` and `*.about.gitlab-review.app` so they do have access to workers
used in other zones.

We are not currently ingesting logs from Cloudflare Workers.

## Performance

Currently, worker performance is not actively monitored or alerted on, however
[reliability#24562](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24562) tracks using metrics that are now
available in the Cloudflare exporter to alert on errors and
latency.

Live metrics can be viewed at:

<https://dash.cloudflare.com/852e9d53d0f8adbd9205389356f2303d/workers/services/view/about-gitlab-com-production/production>

## Monitoring and Alerts

Given that this is a service hosted by Cloudflare backed by GCS, we only have basic monitoring.

We have two external checks:

- Pingdom pings <https://about.gitlab.com> every 5 minutes
- Blackbox probes for several `https://about.gitlab.com/...` endpoints

Both of these checks would alert the EOC if the conditions are met.

For troubleshooting, we have a [runbooks
page](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/about-gitlab-com.md#availability-issues)
with some troubleshooting information.

## Responsibility

This service was migrated from Fastly and built using Cloudflare Workers by the Reliability::General team.
